/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.accsoft.steering.jmad.elena;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import cern.accsoft.steering.jmad.domain.ex.JMadModelException;
import cern.accsoft.steering.jmad.domain.knob.MadxParameter;
import cern.accsoft.steering.jmad.domain.knob.strength.Strength;
import cern.accsoft.steering.jmad.domain.machine.MadxRange;
import cern.accsoft.steering.jmad.domain.machine.Range;
import cern.accsoft.steering.jmad.domain.result.match.MatchResult;
import cern.accsoft.steering.jmad.domain.result.match.MatchResultRequest;
import cern.accsoft.steering.jmad.domain.result.match.MatchResultRequestImpl;
import cern.accsoft.steering.jmad.domain.result.match.input.MadxVaryParameter;
import cern.accsoft.steering.jmad.domain.result.match.input.MadxVaryParameterImpl;
import cern.accsoft.steering.jmad.domain.result.match.input.MatchConstraint;
import cern.accsoft.steering.jmad.domain.result.match.input.MatchConstraintGlobal;
import cern.accsoft.steering.jmad.domain.result.match.input.MatchConstraintLocal;
import cern.accsoft.steering.jmad.domain.result.match.methods.MatchMethod;
import cern.accsoft.steering.jmad.domain.result.match.methods.MatchMethodLmdif;
import cern.accsoft.steering.jmad.domain.result.match.methods.MatchMethodSimplex;
import cern.accsoft.steering.jmad.domain.result.tfs.TfsSummary;
import cern.accsoft.steering.jmad.model.JMadModel;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.service.JMadService;
import cern.accsoft.steering.jmad.service.JMadServiceFactory;

public class ElenaMatchingTest {

    @Test
    public void test() throws JMadModelException {
        JMadService service = JMadServiceFactory.createJMadService();
        List<JMadModelDefinition> mdefs = service.getModelDefinitionManager().getAllModelDefinitions();

        JMadModelDefinition elenaDef = service.getModelDefinitionManager().getModelDefinition("ELENA-2017");

        System.out.println(elenaDef);

        JMadModel elena = service.createModel(elenaDef);
        elena.init();

        TfsSummary tw = elena.calcTwissSummary();
        System.out.println(tw);

        MatchResultRequestImpl request = new MatchResultRequestImpl();

        MatchConstraintGlobal global = new MatchConstraintGlobal();
        MatchConstraintLocal eCoolerDispersion = new MatchConstraintLocal(new MadxRange("EBEAM"));

        /* INPUT */
        global.setQ1(2.41);
        global.setQ2(1.44);
        eCoolerDispersion.setDx(1.3);

        
        
        
        request.addMatchConstraint(global);
        request.addMatchConstraint(eCoolerDispersion);

        request.addMadxVaryParameter(varyStrength(elena, "KQ1"));
        request.addMadxVaryParameter(varyStrength(elena, "KQ2"));
        request.addMadxVaryParameter(varyStrength(elena, "KQ3"));

        request.setMatchMethod(matchMethod());

        MatchResult result = elena.match(request);

        System.out.println("");
        System.out.println("RESULT:");
        System.out.println("=======");
        System.out.println("Final penalty: " + result.getFinalPenalty());
        result.getVaryParameterResults().forEach(r -> {
            System.out.println(r.getName() + "=" + r.getFinalValue());
        });

        System.out.println(elena.calcTwissSummary());
        System.out.println("Final dispersion: " + elena.getOptics().getPointByName("EBEAM").getDx());

    }

    private MadxVaryParameter varyStrength(JMadModel elena, String strName) {
        return new MadxVaryParameterImpl(strOfName(elena, strName));
    }

    private Strength strOfName(JMadModel elena, String strengthName) {
        return elena.getStrengthsAndVars().getStrength(strengthName);
    }

    private MatchMethod matchMethod() {
        MatchMethodLmdif matchMethod = new MatchMethodLmdif();
        matchMethod.setCalls(1000);
        matchMethod.setTolerance(0.00000001);
        return matchMethod;
    }

}
