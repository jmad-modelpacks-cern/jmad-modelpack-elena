/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.accsoft.steering.jmad.modeldefs.def.elena;

import java.util.HashSet;
import java.util.Set;

import cern.accsoft.steering.jmad.MadXConstants;
import cern.accsoft.steering.jmad.domain.beam.Beam;
import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsets;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.filter.RegexNameFilter;
import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;
import cern.accsoft.steering.jmad.domain.types.enums.JMadPlane;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinitionImpl;

public class ElenaModelDefinitionFactory implements ModelDefinitionFactory {
    
private void addInitFiles(JMadModelDefinitionImpl modelDefinition) {
    
    /*
     * call the two strength definition files 
     */
    modelDefinition.addInitFile(new CallableModelFileImpl(
            "strength/elena.str",ModelFileLocation.REPOSITORY));
    modelDefinition.addInitFile(new CallableModelFileImpl(
            "strength/electron_cooler.str",ModelFileLocation.REPOSITORY, ParseType.STRENGTHS));

    /* elements */
    modelDefinition.addInitFile(new CallableModelFileImpl("elements/elena.ele",ModelFileLocation.REPOSITORY));

    /*
     * call the sequence definition file for elena
     */
    modelDefinition.addInitFile(new CallableModelFileImpl("sequence/elena.seq",ModelFileLocation.REPOSITORY));

    /* beam */
    modelDefinition.addInitFile(new CallableModelFileImpl(
            "beams/elena.beam", ModelFileLocation.REPOSITORY));
}

private ModelPathOffsets createModelPathOffsets() {
    ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
    offsets.setResourceOffset("");
    offsets.setRepositoryOffset("2018");
    return offsets;
}

private Set<OpticsDefinition> createOpticsDefinitions(){
    Set<OpticsDefinition> definitionSet = new HashSet<>();
    definitionSet.add( new OpticsDefinitionImpl(
            "Nominal-2018", new CallableModelFileImpl(
                    "strength/elena.str",
                    ModelFileLocation.REPOSITORY, ParseType.STRENGTHS)));
    return definitionSet;
}

    @Override
    public JMadModelDefinition create() {
        JMadModelDefinitionImpl modelDefinition = new JMadModelDefinitionImpl();
        
        modelDefinition.setName("ELENA");
        modelDefinition.setModelPathOffsets(createModelPathOffsets());
        

        this.addInitFiles(modelDefinition);
        
        for(OpticsDefinition opticsDefinition :  createOpticsDefinitions()) {
        modelDefinition.addOpticsDefinition(opticsDefinition);
        if(opticsDefinition.getName()=="Nominal-2018") 
            modelDefinition.setDefaultOpticsDefinition(opticsDefinition);
        }
        
        /*
         * SEQUENCE
         */
        SequenceDefinitionImpl elenaSeq = new SequenceDefinitionImpl("elena", createBeam());
        modelDefinition.setDefaultSequenceDefinition(elenaSeq);


        TwissInitialConditionsImpl twiss = new TwissInitialConditionsImpl(
                "default-twiss");
        twiss.setCalcAtCenter(true);
        twiss.setCalcChromaticFunctions(true);
        RangeDefinitionImpl range = new RangeDefinitionImpl(elenaSeq, "ALL", twiss);
        range.addCorrectorInvertFilter(new RegexNameFilter(".*", JMadPlane.H));
        range.addCorrectorInvertFilter(new RegexNameFilter(".*", JMadPlane.V));
        elenaSeq.setDefaultRangeDefinition(range);
        return modelDefinition;
    }
    
    public static Beam createBeam() {
        Double energy = 20.0; // energy in GeV
        Double gamma = energy / MadXConstants.MASS_PROTON; // beta
        Double emittance = 3.5e-06; // normalized emittance
        Double xEmittance = emittance / (gamma);
        Double yEmittance = emittance / (gamma);

        Beam beam = new Beam();
        beam.setParticle(Beam.Particle.ANTIPROTON);
        beam.setEnergy(energy);
        beam.setBunchLength(0.077);
        beam.setDirection(Beam.Direction.PLUS);
        beam.setParticleNumber(1.1E11);
        beam.setRelativeEnergySpread(5e-4);
        beam.setHorizontalEmittance(xEmittance);
        beam.setVerticalEmittance(yEmittance);
        return beam;
    }
}


