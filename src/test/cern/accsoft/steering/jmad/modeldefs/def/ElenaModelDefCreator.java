/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.accsoft.steering.jmad.modeldefs.def;

import static cern.accsoft.steering.jmad.tools.modeldefs.creating.ModelDefinitionCreator.scanDefault;

public class ElenaModelDefCreator {

    public static void main(String[] args) {
        scanDefault().and().writeToDefault();
    }

}
