/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.accsoft.steering.jmad.modeldefs.def;

import static cern.accsoft.steering.jmad.tools.modeldefs.cleaning.ModelPackageCleaner.cleanUnusedBelow;

public class ElenaModelDefCleaner {

    public static void main(String[] args) {
       cleanUnusedBelow("src/java");

    }

}
